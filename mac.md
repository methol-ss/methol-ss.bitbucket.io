
## mac

#### 下载软件
[ShadowsocksX-NG.app.1.8.2.zip](mac/ShadowsocksX-NG.app.1.8.2.zip)
所有版本在 [https://d.methol.app](https://d.methol.app)，如需要其他版本，可以自行下载安装  
下载后解压，拖动**.app**文件到**应用程序**目录，之后打开在**launchpad**可以看到该文件，打开即可。

#### 软件正式使用
1. 安装软件后打开，可以看到menu bar有一个小飞机的图标。  
     ![](img/5b2f7343991f5.png)  
2. 打开[**账号**](https://ssm.methol.app:666/user/account) [https://ssm.methol.app:666/user/account](https://ssm.methol.app:666/user/account) 页面，然后点击页面上二维码，会弹出一个窗口。最后再点击弹出窗口的二维码，可以自动导入配置到软件中。  
   ![](img/5b2f76fcebbcd.png)  
3. 有通知消息提示导入成功，如下图。    
   ![](img/5b2f74ac3d6a1.png)  
4. 切换服务器，二维码会改变，再执行2-3的操作导入其他服务器配置。  
   ![](img/5b2f769eb4a4e.png)  
5. 点击menu bar的小飞机图标，再选择**从gfwlist更新pac地址**，更新成功后会有通知消息提示更新成功。    
   ![](img/59705888a3966.jpg)  
6. 代理模式选择 **pac自动模式**，启用代理，即可使用。  
7. 点击**服务器**可以切换服务器。请第一次配置的时候添加所有服务器，方便其中一个服务器挂了可以随时切换，同时也可以自己一个个试，找一个最快的。  
   ![](img/5b2f7792d0a19.png)  
