# 推荐把本页加入书签保存！

## 管理后台：[https://ssm.methol.app:666](https://ssm.methol.app:666/)

## 教程：[https://d.methol.app/manaual.html](https://d.methol.app/manaual.html)

### windows(支持订阅，推荐，教程)
[ssd-0.1.7.zip](windows/ssd/ssd-0.1.7.zip)

### windows(不支持订阅，需要手动导入配置，不推荐使用)
[Shadowsocks-4.1.3.1.zip](windows/Shadowsocks-4.1.3.1.zip)
[Shadowsocks-4.1.3.zip](windows/Shadowsocks-4.1.2.zip)
[Shadowsocks-4.1.2.zip](windows/Shadowsocks-4.1.2.zip)
[Shadowsocks-4.1.1.zip](windows/Shadowsocks-4.1.1.zip)
[Shadowsocks-4.1.0.zip](windows/Shadowsocks-4.1.0.zip)
[Shadowsocks-4.0.zip](windows/Shadowsocks-4.0.zip)
[Shadowsocks-4.0.9.zip](windows/Shadowsocks-4.0.9.zip)
[Shadowsocks-4.0.8.zip](windows/Shadowsocks-4.0.8.zip)
[Shadowsocks-4.0.7.zip](windows/Shadowsocks-4.0.7.zip)
[Shadowsocks-4.0.6.zip](windows/Shadowsocks-4.0.6.zip)
[Shadowsocks-4.0.5.zip](windows/Shadowsocks-4.0.5.zip)
[Shadowsocks-4.0.4.zip](windows/Shadowsocks-4.0.4.zip)
[Shadowsocks-4.0.2.zip](windows/Shadowsocks-4.0.2.zip)
[Shadowsocks-4.0.10.zip](windows/Shadowsocks-4.0.10.zip)
[Shadowsocks-4.0.1.zip](windows/Shadowsocks-4.0.1.zip)
[Shadowsocks-3.4.3.zip](windows/Shadowsocks-3.4.3.zip)
[Shadowsocks-3.4.2.zip](windows/Shadowsocks-3.4.2.zip)
[Shadowsocks-3.4.2.1.zip](windows/Shadowsocks-3.4.2.1.zip)
[Shadowsocks-3.3.zip](windows/Shadowsocks-3.3.zip)
[Shadowsocks-3.3.6.zip](windows/Shadowsocks-3.3.6.zip)
[Shadowsocks-3.3.5.zip](windows/Shadowsocks-3.3.5.zip)
[Shadowsocks-3.3.4.zip](windows/Shadowsocks-3.3.4.zip)
[Shadowsocks-3.3.3.zip](windows/Shadowsocks-3.3.3.zip)
[Shadowsocks-3.3.2.zip](windows/Shadowsocks-3.3.2.zip)
[Shadowsocks-3.3.1.zip](windows/Shadowsocks-3.3.1.zip)
[Shadowsocks-3.2.zip](windows/Shadowsocks-3.2.zip)
[Shadowsocks-3.0.zip](windows/Shadowsocks-3.0.zip)
[Shadowsocks-2.5.8.zip](windows/Shadowsocks-2.5.8.zip)
[Shadowsocks-2.5.7.zip](windows/Shadowsocks-2.5.7.zip)
[Shadowsocks-2.5.6.zip](windows/Shadowsocks-2.5.6.zip)

#### windows软件运行环境
[vc_redist.x64.exe](windows/vc_redist.x64.exe)
[win_net_framework.exe](windows/NDP-Web.exe)

### mac
[ShadowsocksX-NG.app.1.8.2.zip](mac/ShadowsocksX-NG.app.1.8.2.zip)
[ShadowsocksX-NG.1.8.0.zip](mac/ShadowsocksX-NG.1.8.0.zip)
[ShadowsocksX-NG.1.7.1.zip](mac/ShadowsocksX-NG.1.7.1.zip)
[ShadowsocksX-NG.1.7.0.zip](mac/ShadowsocksX-NG.1.7.0.zip)
[ShadowsocksX-NG.1.6.1.zip](mac/ShadowsocksX-NG.1.6.1.zip)
[ShadowsocksX-NG.1.6.0.zip](mac/ShadowsocksX-NG.1.6.0.zip)
[ShadowsocksX-NG.1.5.1.zip](mac/ShadowsocksX-NG.1.5.1.zip)
[ShadowsocksX-NG.1.5-beta.2.zip](mac/ShadowsocksX-NG.1.5-beta.2.zip)
[ShadowsocksX-NG.1.5-alpha.zip](mac/ShadowsocksX-NG.1.5-alpha.zip)
[ShadowsocksX-NG-1.5.0.zip](mac/ShadowsocksX-NG-1.5.0.zip)
[ShadowsocksX-NG-1.4.zip](mac/ShadowsocksX-NG-1.4.zip)
[ShadowsocksX-NG-1.4-beta.zip](mac/ShadowsocksX-NG-1.4-beta.zip)
[ShadowsocksX-NG-1.4-alpha.zip](mac/ShadowsocksX-NG-1.4-alpha.zip)
[ShadowsocksX-NG-1.3.2.zip](mac/ShadowsocksX-NG-1.3.2.zip)
[ShadowsocksX-NG-1.3.1.dmg](mac/ShadowsocksX-NG-1.3.1.dmg)
[ShadowsocksX-NG-1.2.dmg](mac/ShadowsocksX-NG-1.2.dmg)
[ShadowsocksX-NG-1.1.dmg](mac/ShadowsocksX-NG-1.1.dmg)

### ios
#### Shadowrocket(教程使用这个)
[Shadowrocket-2.1.23](itms-services://?action=download-manifest&url=https://d.methol.app/ios/Shadowrocket-2.1.23.plist)
#### 其他通用客户端，请自行探索
[Quantumult-2.2.8](itms-services://?action=download-manifest&url=https://d.methol.app/ios/Quantumult-2.2.8.plist)
[Potatso2-2.9.0](itms-services://?action=download-manifest&url=https://d.methol.app/ios/Potatso2-2.9.0.plist)

### android手机版
[shadowsocks--universal-4.6.3.apk](android/shadowsocks--universal-4.6.3.apk)
[shadowsocks--universal-4.6.2.apk](android/shadowsocks--universal-4.6.2.apk)
[shadowsocks--universal-4.6.1.apk](android/shadowsocks--universal-4.6.1.apk)
[shadowsocks--universal-4.6.0.apk](android/shadowsocks--universal-4.6.0.apk)
[shadowsocks--universal-4.5.7.apk](android/shadowsocks--universal-4.5.7.apk)
[shadowsocks--universal-4.5.6.apk](android/shadowsocks--universal-4.5.6.apk)
[shadowsocks--universal-4.5.5.apk](android/shadowsocks--universal-4.5.5.apk)
[shadowsocks--universal-4.5.4.apk](android/shadowsocks--universal-4.5.4.apk)
[shadowsocks--universal-4.5.3.apk](android/shadowsocks--universal-4.5.3.apk)
[shadowsocks--universal-4.5.2.apk](android/shadowsocks--universal-4.5.2.apk)
[shadowsocks--universal-4.5.1.apk](android/shadowsocks--universal-4.5.1.apk)
[shadowsocks--universal-4.5.0.apk](android/shadowsocks--universal-4.5.0.apk)
[shadowsocks--universal-4.4.6.apk](android/shadowsocks--universal-4.4.6.apk)

### android电视版
[shadowsocks-tv--universal-4.6.3.apk](android/shadowsocks-tv--universal-4.6.3.apk)
[shadowsocks-tv--universal-4.6.2.apk](android/shadowsocks-tv--universal-4.6.2.apk)


### 远程工具
[anydesk.dmg](yuancheng/anydesk.dmg)
[AnyDesk.exe](yuancheng/AnyDesk.exe)