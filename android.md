
## android
#### 下载软件
[shadowsocks--universal-4.6.1.apk](android/shadowsocks--universal-4.6.1.apk)
所有版本在 [https://d.methol.app](https://d.methol.app)，如需要其他版本，可以自行下载安装  

#### 软件正式使用
1. 下载安装软件，安装后如下，一个小飞机的图标  
   ![](img/5b2facae235e7.png)  
2. 打开软件，删除软件自带的广告服务器，左滑或者右滑即可删除。  
   ![](img/5b2facfac8203.png)  
3. 用手机自带浏览器打开ss账号管理网站[https://ssm.methol.app:666](https://ssm.methol.app:666)，并登陆  
   ![](img/5b2fad30ca0f0.png)
4. 打开账号页面[https://ssm.methol.app:666/user/account](https://ssm.methol.app:666/user/account)，并点击下方二维码  
   ![](img/5b2fad90c9b14.png)  
5. 再次点击放大弹出窗口的二维码

   ![](img/5b2fadbe0320d.png)

6. 此时会跳转到软件，点击确认

   ![](img/5b2fadeb4e668.png)

7. 导入成功后，点击右侧笔的按钮修改配置

   ![](img/5b2fae6d4b85f.png)

8. 修改 “功能设置” - “路由” ，选择“GFW 列表”，然后点击右上角保存。

   ![](img/5b2faf1e19094.png)

9. 先选中服务器，然后点击右下角飞机按钮，即可启动，此时状态栏会有一个钥匙标记。

   ![](img/5b2fafa5a847c.png)

10. 回到浏览器账号页面。切换服务器，二维码会改变，再执行4-9的操作导入其他服务器配置。

    ![](img/5b2f769eb4a4e.png)

11. 全部导入节点后截图如下。点击任何一个节点可以切换服务器。**请第一次配置的时候添加所有服务器**，方便其中一个服务器挂了可以随时切换，**同时也可以自己一个个试，找一个最快的**。  

    有一点需要注意的是，每个服务器都需要改一下，**路由选择GFW列表**，不要使用全局代理。

    ![](img/5b2fb05b93ba0.jpg)
