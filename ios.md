
## ios (使用前请先联系我确认在线，需要获取验证码)
#### 下载软件
使用**safari**打开本网页，点击如下链接，看到弹窗后点击安装。
[Shadowrocket-2.1.23](itms-services://?action=download-manifest&url=https://d.methol.app/ios/Shadowrocket-2.1.23.plist)
手机会多出一个图标(显示**等待中...**)，稍等一下，即可安装成功。
如果一直没有变化，点击该图标(显示**正在载入...**)，等加载完成即可安装成功。  
其他第三方客户端在 [https://d.methol.app](https://d.methol.app)，如需要其他版本，可以自行下载安装  

#### 软件正式使用
1. 打开**shadowrocket**(小火箭图标)这个软件，第一次一般需要提示输入账号和密码。  
   输入账号和密码登录我的账号  
   
   账号 **ios@tuzhihao.com**
   密码 **1993TUzhihao**   
   
   然后按提示操作。
2. 双重认证这里，先点击**没有收到验证码**

   ![](img/5b2f7b3ca4d80.png)

3. 选择**发送代码至电话号码**

   ![](img/5b2f7b7d1d08f.png)

4. 选**尾数为58**的这个电话，点击后联系我获取验证码。

   ![](img/5b2f7bb0b99aa.png)
5. 验证码输入成功后，你应该已经可以打开软件了。此时，先不要操作软件。  
   登陆ss账号管理的后台网站(https://ssm.methol.app:666/)

   ![](img/5b2f7c9859165.png)

6. 打开账号页面[https://ssm.methol.app:666/user/account](https://ssm.methol.app:666/user/account)，滑倒最下面，点击「订阅链接」按钮，会弹出框，选择**shadowrocket**点击「复制链接」

    ![](img/5b38e4b8c0592.png)

7. 打开 shadowrocket 那个软件，点击「右上角的 +」 按钮，类型选择 **Subscrible**，**url**填写刚刚复制的，直接粘贴进去就好，然后点击右上角的「完成

    ![](img/5b38e4dc540fa.png)

8. 添加完成后如下图所示，会自动同步所有的服务器信息，点击服务器的名字会自动切换到该服务器代理。

    ![](img/5b38e50137200.png)

9. **强烈建议**更改如下设置：在shadowrocket这个软件内，『设置』-『服务器订阅』-**打开时更新**(选中该项)。它会在每次打开该软件的时候获取最新的服务器信息。


