
## windows

#### 下载软件
[ssd-0.1.7.zip](windows/ssd/ssd-0.1.7.zip)
所有版本在 [https://d.methol.app](https://d.methol.app)，如需要其他版本，可以自行下载安装

#### 软件运行环境安装
- win7、win8   
  先安装最新的MicroSoft .net framework，点击下面的地址，下载并安装，时间可能要久一点。  
  [win_net_framework.exe](windows/NDP-Web.exe). 
- win10 忽略该步骤，直接查看软件正式使用部分
- xp不支持  

#### 软件正式使用
1. 下载软件，然后**新建一个ss的文件夹**(路径不限，例子中使用桌面)，如下图。   
   ![F63CC64903D8AE1AEE88E14183B1A88E](media/F63CC64903D8AE1AEE88E14183B1A88E.jpg)

2. 把zip文件解压到新建的ss文件夹，解压后的文件是**ShadowsocksD.exe**，如下图  
  ![05E02834E1FACA13E1E115C050B659FD](media/05E02834E1FACA13E1E115C050B659FD.png)

3. 双击打开这个软件，右下角托盘区，出现了一个小飞机的图标。  
   ![4830A423F8E91570AB03856DAEEA2F25](media/4830A423F8E91570AB03856DAEEA2F25.png)

4. 登录ss管理网站，打开[**账号**](https://ssm.methol.app:666/user/account) [https://ssm.methol.app:666/user/account](https://ssm.methol.app:666/user/account) 页面，如下图：  
   ![-w496](media/15455785918304.jpg)

5. 点击订阅链接按钮，选择**ssd**这一项，然后点击复制链接
   ![-w612](media/15455786716118.jpg)

6. 右键托盘区小飞机按钮，选择 **订阅** -> **管理**
![-w324](media/15455792243565.jpg)

7. 会出现如下弹窗，订阅链接粘贴刚才复制的链接  
   ![9D40795F6C30795BE1C9F6451003787E](media/9D40795F6C30795BE1C9F6451003787E.png)
  
  点击添加后，左侧会多出 **ssmgr** 这一栏，点击这一栏，会显示到期时间等信息。
  ![3D4FD897415FC2C8AEB9655624ED7A4B](media/3D4FD897415FC2C8AEB9655624ED7A4B.png)
  
  **建议定期更新，以获取最新的服务器信息！**
  
8. 右键托盘区小飞机，选择 **服务器** -> **ssmgr** -> ，从多个服务器选择一个，第一列 **xxms** 表示延迟，理论上延迟越低速度越快！
![-w574](media/15455791278715.jpg)

8. 右键点击托盘区的小飞机，选择"PAC" - "从 GFWList 更新本地PAC" 
   ![](img/5b2fb5e62b4be.png)
   
9. 更新成功后会有气泡提示，**如果失败了，请确认上面的操作没问题，或者联系我**。  
   如下图表示更新成功  
   ![](img/5b2fb64c16d12.png)  
   如下图表示更新失败  
   ![](img/5b2fb669accc2.png)  
10. 右键小飞机，勾选启用代理，并且系统代理模式选择**PAC模式**，此时即可正常使用。  
   ![](img/5b2fb6c27bc4f.png)  

10. win用户建议勾选开机启动，选择pac模式的时候，他会自动代理被屏蔽的网站，访问baidu之类的就不会代理，所以它一直开着就好  
    ![](img/5b2fb824b01c9.png)  
